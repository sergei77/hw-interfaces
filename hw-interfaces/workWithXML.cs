﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;

// --------------------------------------------------------------------------------------------------------------------------- 
// Увидев это не стал создавать отдельный класс
// "когда реализуете этот метод, используйте yield (его можно использовать просто в методе, без создания отдельного класса)"
// --------------------------------------------------------------------------------------------------------------------------- 


namespace hw_interfaces
{
    public interface ISerializer<T>
    {
        string Serialize(T item);
        T Deserialize(string data);
    }

    public class OtusXmlSerializer<T> : ISerializer<T>
    {
        private readonly XmlSerializer formatter = new XmlSerializer(typeof(T));
        public string Serialize(T item)
        {
            using StringWriter sw = new StringWriter();
            formatter.Serialize(sw, item);
            return sw.ToString();
        }

        public T Deserialize(string data)
        {
            using TextReader reader = new StringReader(data);
            return (T)formatter.Deserialize(reader);
        }
    }

    public class OtusStreamReader<T> : IEnumerable<T>, IDisposable
    {
        private readonly StreamReader _streamReader;
        private readonly ISerializer<T> _deserializer;

        private readonly List<T> _elements = new List<T>();
        private bool _elementsReaded = false;

        private bool _disposed = false;
        private bool _streamReaderClosed = false;

        public OtusStreamReader(StreamReader streamReader, ISerializer<T> Deserializer)
        {
            _streamReader = streamReader;
            _deserializer = Deserializer;
        }

        public IEnumerator<T> GetEnumerator()
        {
            // Можно было бы сразу в конструкторе заполнить _elements и здесь просто по циклу выдавать элементы.
            // Но в задании указано: десериалицация в методе GetEnumerator.

            if (!_elementsReaded)
            {
                string line;
                var strForObj = new StringBuilder(256);

                while (!_streamReader.EndOfStream)
                {
                    line = _streamReader.ReadLine();

                    if (line == Constants.ObjDelimiterInFile)
                    {
                        string xml_element = strForObj.ToString();
                        strForObj.Clear();

                        T item = _deserializer.Deserialize(xml_element);
                        _elements.Add(item);                        
                    }
                    else
                        strForObj.Append(line);
                }

                _elementsReaded = true;

                _streamReader.Close();
                _streamReaderClosed = true;
            }

            for (int i = 0; i < _elements.Count; i++)
            {
                yield return _elements[i];
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    // TODO: освободить управляемое состояние (управляемые объекты)
                }

                // Здесь освобождаю неуправляемые ресурсы (неуправляемые объекты).            
                // (переопределить метод завершения?) установить значение NULL для больших полей.
                if (!_streamReaderClosed)
                {
                    _streamReader.Close();
                    _streamReaderClosed = true;
                }

                _disposed = true;
            }
        }

        // добавляю финализатор т.к. использую неуправляемые ресурсы ("Dispose(bool disposing)" содержит код для освобождения неуправляемых ресурсов).
        ~OtusStreamReader()
        {
            // Код очистки в методе "Dispose(bool disposing)".
            Dispose(disposing: false);
        }

        public void Dispose()
        {
            // Код очистки в методе "Dispose(bool disposing)".
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
