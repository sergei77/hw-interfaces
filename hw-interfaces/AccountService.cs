﻿using hw_interfaces.Entities;
using hw_interfaces.RepositoryLevel;
using System;

namespace hw_interfaces
{
    public interface IAccountService
    {
        // В классе-реализаторе делать валидацию: проверить что имена не пустые, что возраст > 18 лет, можете также добавить свои правила
        // Если валидация прошла успешно, то добавлять аккаунт в репозиторий
        void AddAccount(Account account);
    }

    public class AccountService : IAccountService
    {
        // при необходимости можно будет менять на другой репозиторий реализующий IAccountService
        // Всю работу с репозитарием вести соотв. через _repository
        private readonly IRepository<Account> _repository;

        public AccountService(IRepository<Account> repository)
        {
            _repository = repository;
        }

        private bool IsValidAccount(Account account)
        {
            if (account.FirstName == string.Empty)
                return false;

            if (account.LastName == string.Empty)
                return false;

            var age = DateTime.Now.Year - account.BirthDate.Year;

            if (age < 18 || age > 120)
                return false;

            return true;

            //return ( account.FirstName != "" && account.LastName != "" && age > 18 );
        }

        public void AddAccount(Account account)
        {            
            if (IsValidAccount(account))
            {
                _repository.Add(account);
            }
            else
            {
                Console.WriteLine("Запись");
                Console.WriteLine(account);
                Console.WriteLine("не была добавлена (не валидна)");
            }
        }

        public void ShowAll()
        {
            Console.WriteLine("Выдано методом репозитория GetAll()");
            Console.WriteLine("FirstName:     | LastName:    |    BirthDate:     ");

            var l = _repository.GetAll();
            foreach (Account account in l)
                Console.WriteLine(account);

            Console.WriteLine();
        }

        public void GetAndShowOne(Func<Account, bool> predicate, string predicateMethodName)
        {
            Account account = GetOne(predicate);
            if (account != null)
            {
                Console.WriteLine($"Выдан методом репозитория GetOne(), predicate: {predicateMethodName}");
                Console.WriteLine(account);
            }
            else
            {
                Console.WriteLine($"Метод GetOne не нашел записи подходящей указанныму делегату, predicate: {predicateMethodName}");
            }
        }

        public Account GetOne(Func<Account, bool> predicate)
        {
            return _repository.GetOne(predicate);
        }
    }
}
