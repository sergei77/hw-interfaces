﻿using hw_interfaces.Entities;
using System;
using System.Collections.Generic;
using System.IO;

namespace hw_interfaces.RepositoryLevel
{
    // В классе реализаторе сохранять данные в какой-нибудь файл. Формат на ваше усмотрение - json, xml, csv, etc
    public class MyRepository<T> : IRepository<T>
    {
        public void Add(T item)
        {
            var otusXmlSerializer = new OtusXmlSerializer<T>();

            try
            {
                using StreamWriter wr = new StreamWriter(Constants.NameOfFileWithObjs, true);
                wr.WriteLine(otusXmlSerializer.Serialize(item));
                wr.WriteLine(Constants.ObjDelimiterInFile);
            }
            catch
            {
                Console.WriteLine("Исключение в: public void Add( T item )");
            }
        }

        public IEnumerable<T> GetAll()
        {
            if (File.Exists(Constants.NameOfFileWithObjs))
            {
                var sr = new StreamReader(Constants.NameOfFileWithObjs);
                OtusStreamReader<T> otusStreamRd = new OtusStreamReader<T>(sr, new OtusXmlSerializer<T>());

                foreach (T prs in otusStreamRd)
                    yield return prs;
            }
        }

        public T GetOne(Func<Account, bool> predicate)
        {
            if (File.Exists(Constants.NameOfFileWithObjs))
            {
                var sr = new StreamReader(Constants.NameOfFileWithObjs);
                var otusStreamRd = new OtusStreamReader<T>(sr, new OtusXmlSerializer<T>());

                foreach (T prs in otusStreamRd)
                    if (predicate(prs as Account)) return prs;
            }

            return default;
        }
    }
}
