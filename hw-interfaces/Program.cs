﻿using hw_interfaces.Entities;
using hw_interfaces.RepositoryLevel;
using System;

namespace hw_interfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("---  Для объектов Person сериализация, десериализация, сортировка ----" + Environment.NewLine);
                Auxiliary.ShowForPersons();
                Console.WriteLine("\nНажмите Enter для просмотра работы с классом Account");
                Console.ReadLine();

                Console.WriteLine("---  Для объектов Account (для сериализации, десериализации используются те же методы) ----" + Environment.NewLine);
                Console.WriteLine("---  Добавление Account-в в базу ----" + Environment.NewLine);
                Auxiliary.ClearPepository();
                var accSevice = new AccountService(new MyRepository<Account>());
                accSevice.AddAccount(new Account("Шибанов", "Сергей", new DateTime(1977, 10, 27)));
                accSevice.AddAccount(new Account("Иванов", "Порфирий", new DateTime(2000, 7, 19)));
                accSevice.AddAccount(new Account("Шибанов", "Иван", new DateTime(2009, 5, 7)));
                accSevice.AddAccount(new Account("Петров", "Петр", new DateTime(2001, 7, 21)));
                accSevice.AddAccount(new Account("Сидоров", "Аркадий", new DateTime(1990, 6, 20)));
                accSevice.AddAccount(new Account("Васечкин", "Василий", new DateTime(1994, 7, 11)));
                accSevice.AddAccount(new Account("", "Некто", new DateTime(1980, 5, 20)));
                accSevice.AddAccount(new Account("Иванова", "Юлия", new DateTime(1988, 4, 23)));

                Console.WriteLine("---  Добавление Account-в произведено ----" + Environment.NewLine);

                accSevice.ShowAll();

                accSevice.GetAndShowOne(Predicates<Account>.SelectItemAboutMe, "SelectItemAboutMe");
                accSevice.GetAndShowOne(Predicates<Account>.SelectItem, "SelectItem");
                Console.WriteLine(Environment.NewLine + "Нажмите Enter для завершения программы");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.ReadLine();
        }
    }
}

