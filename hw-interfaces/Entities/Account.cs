﻿using System;


namespace hw_interfaces.Entities
{
    [Serializable]
    public class Account
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }

        // стандартный конструктор без параметров (для штатного сериализатора).
        public Account()
        { }

        public Account(string FirstName, string LastName, DateTime BirthDate)
        {
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.BirthDate = BirthDate;
        }

        public override string ToString()
        {
            return $"  {FirstName,-16}  {LastName,-16}  {BirthDate:d}";
        }
    }
}
