﻿namespace hw_interfaces
{
   public static class Constants
   {
      //  с разделителем реализовано, чтобы можно было добавлять записи в файл не считывая и перезаписывая его весь.
      public const string ObjDelimiterInFile = "--- obj separator ----";

      public static readonly string NameOfFileWithObjs = System.IO.Path.GetFullPath(@"..\..\") + "\\persons.txt";
   }
}

