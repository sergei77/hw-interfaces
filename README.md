Домашнее задание
Создаём набор классов и их интерфейсов
Цель: В этот ДЗ студент создаст набор классов и их интерфейсов
Задание 1:
Реализация IEnumerable<T> на примере чтения списка элементов из xml, json ил сsv файла.
В качестве десериализатора можно использовать https://github.com/ExtendedXmlSerializer/home или http://csvhelper.com/

Здесь более пошагово на примере xml и некоего класса Person:
https://pastebin.com/6FJZanYv


Задание 2:
Создать интерфейс IAlgorithm, добавить в него метод. Например, сортировка. Применить интерфейс к классу из первого задания.
(Помните, что реализовав IEnumerable<T>, для вашего класса становятся доступны методы LINQ?)

Задание 3:
Реализуйте интерфейсы из https://gist.github.com/viktor-nikolaev/46e588fc1c52bbf03186597af23fcd61
Напишите тесты IAccountService.AddAcount() с использованием Moq. 


Из https://pastebin.com/6FJZanYv
Вам нужно сделать десериализацию ваших объектов из xml файла. (Можно добавить метод Deserialize в ваш интерфейс ISerializer)

Сам класс Person не должен ничего знать о сериализаторах и о том, как он хранится.

Примерно такая архитектура/классы должны быть:
OtusXmlSerializer<T> : ISerializer<T>
OtusStreamReader<T> : IEnumerable<T>, IDisposable
PersonSorter : ISorter<T>
Person

Более подробно:
* ISerializer<T>
Имеет два метода:
string Serialize<T>(T item);
T Deserialize<T>(Stream stream);

если лень работать со стримами, можете Deserialize заменить на:
T Deserialize<T>(string data);
Но тогда в  OtusStreamReader тоже используйте строки.

* OtusXmlSerializer<T> : ISerializer<T>
для реализации методов интерфейса используем этот пакет https://www.nuget.org/packages/ExtendedXmlSerializer/
Примеры работы описаны здесь https://extendedxmlserializer.github.io/documentation/conceptual/documents/The-Basics.html
Также на проекте с урок в гитхабе есть пример https://github.com/Veikedo/Otus.Interfaces/blob/master/Otus.Interfaces/2.%20Strategy.cs#L25

* OtusStreamReader<T>
В конструкторе получаем Stream и ISerializer<T>. В методе GetEnumerator() вызываем serializer.Deserialize<T[]>(Stream) и проходимся в цикле по массиву, возвращая элементы с помощью yield return. 
Да, можно было бы просто вернуть массив, но мы заодно научимся работать с yield.

* ISorter<T> 
имеет один метод 
IEnumerable<T> Sort<T>(IEnumerable<T> notSortedItems);

* PersonSorter : ISorter<Person>
реализует метод Sort<Person>. Можно просто вызвать метод Linq
